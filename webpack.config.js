var path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
var plugins = [
    new HtmlWebpackPlugin({
        template: './HTML/MyNews.html',
        filename: 'HTML/MyNews.html',
        chunks: ['MyNews']
    })
];
module.exports = {
    entry: {
        MyNews: [path.resolve(__dirname, 'APP/MyNews.jsx')],
    },
    output: {
        filename: 'js/[name].js',
        path: path.resolve(__dirname, 'dist'),
        assetModuleFilename: 'asset/images/[name][hash:4][ext]',
    },
    mode: 'production',
    target: ['web', 'es5'],
    plugins: plugins,
    module: {
        rules: [
            {
                test: /\.js|jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            ['@babel/preset-react', { "runtime": "automatic" }]
                        ]
                    }

                }
            },
            {
                test: /\.scss$/,
                use: [{ loader: "style-loader" }, { loader: "css-loader" }, { loader: "sass-loader" }]

            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            // {
            //     test: /\.svg$/,
            //     use: {
            //         loader: 'svg-url-loader',
            //         options: {
            //             limit: 10000,
            //         },
            //     }
            // },
            {
                test: /\.(png|jpg|gif|svg)$/,
                type: 'asset',
                // generator:{
                //     filename : 'asset/images/[name][ext]'
                // }
                // parser: {
                //   dataUrlCondition: {
                //     maxSize: 10 * 1024 // 4kb
                //   }
                // }
            },
        ]
    },
    // devServer: {
    //     port: 8888,
    //     hot: true, // 热重载
    //     open: true, // 自定打开浏览器
    // },
    // performance: {
    //     hints: false,
    //     maxEntrypointSize: 512000,
    //     maxAssetSize: 512000
    // }
}