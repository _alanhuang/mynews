//Router-----------------------------------

    使用Link時可以使用NavLink選中時會加上active class方便做style控制
    
//Router-----------------------------------


//styled-system-------------------------------

    import { color } from 'styled-system'
        有兩個屬性可以用　color(設置字體顏色) 和 bg(backgroundColor)(背景顏色) 固定用法
            <Test color="white" bg="black" />
        也可以直接設置ThemeProvider
            const Test = styled.div`
                ${color};
            `;

//styled-system-------------------------------


//scss--------------------------------------
    網路參考模組化管理
    (有底線代表不會被編譯成一支檔，合併使用)
    _variable.scss //專門放相關變數
    _mixin.scss //專門放相關mixin函式
    _mixinRwd.scss //rwd media
    _base.scss //全域設定SCSS
    _index.scss //專案頁面SCSS
//scss--------------------------------------