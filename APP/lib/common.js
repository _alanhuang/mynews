import $ from 'jquery';
import axios from 'axios';

let AppName = '';

// if (window.location.hostname == 'localhost' || window.location.hostname == '127.0.0.1') {
// } else {
//     AppName = '/ReactProject';
// }

export const getApi = (url, params) => {
    let result;
    $.ajax({
        url: url,
        type: "get",
        data: params,
        async: false,
        success: function (Jdata) {
            switch (Jdata.Code.toString()) {
                case '0':
                    result = Jdata.Data;
                    break;
                default:
                    alert("資料連線失敗！\n" + Jdata.Message);
                    break;
            }
        },
        error: function (Jdata) {
            alert("資料連線失敗！\n" + Jdata.responseJSON.Message);
        }
    });
    return result;
    // return axios.create({ baseURL: AppName + url })
};



//日期相減換算
export const DateDiff = function (dtStart, dtEnd, strInterval) {
    if (typeof dtStart == 'string')//如果是字串轉換為日期型 
    {
        dtStart = parseDate(dtStart);
    }
    if (typeof dtEnd == 'string')//如果是字串轉換為日期型 
    {
        dtEnd = parseDate(dtEnd);
    }
    switch (strInterval) {
        case 's': return parseInt((dtEnd - dtStart) / 1000);
        case 'n': return parseInt((dtEnd - dtStart) / 60000);
        case 'h': return parseInt((dtEnd - dtStart) / 3600000);
        case 'd': return parseFloat((dtEnd - dtStart) / 86400000);
        case 'w': return parseInt((dtEnd - dtStart) / (86400000 * 7));
        case 'm': return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - dtStart.getFullYear()) * 12) - (dtStart.getMonth() + 1);
        case 'y': return dtEnd.getFullYear() - dtStart.getFullYear();
    }
}

export const currentNewsType = (newstype) => {
    let result = '';
    switch (newstype) {
        case 'focus':
            result = '焦點';
            break;
        case 'entertainment':
            result = '娛樂';
            break;
        case 'domestic':
            result = '國內';
            break;
    }
    return result;
}

export const DateCalc = (date, startStr) => {
    if (!startStr) {
        startStr = '';
    }
    let calcDate = DateDiff(new Date(date), new Date(), 'n');
    if (calcDate == 0) {
        calcDate = `${startStr} 剛剛`;
    } else if (calcDate >= 60 && calcDate < 1440) {
        calcDate = `${startStr} ${parseInt(DateDiff(new Date(date), new Date(), 'h'))} 小時前`;
    } else if (calcDate >= 1440) {
        calcDate = `${startStr} ${parseInt(DateDiff(new Date(date), new Date(), 'd'))} 天前`;
    } else {
        calcDate = `${startStr} ${parseInt(DateDiff(new Date(date), new Date(), 'n'))} 分鐘前`;
    }
    return calcDate;
}

export const Storages = (key) => {
    let val = localStorage[key];
    if (!val) {
        val = "";
    }
    return val;
}

export const SetStorages = (key, val) => {
    localStorage.setItem(key, val);
}