import { createStore, combineReducers } from 'redux';
import myNews from './myNews';

const reducer = combineReducers({
    myNews
});

export function configureStore(initialState) {
    const Reducer = createStore(reducer, initialState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
    return Reducer;
}