import URI from 'urijs';
import { Storages } from '../lib/common';
var uri = new URI();

export function getTheme() {
    let theme = Storages("theme");
    if (theme == '') {
        return 'whiteTheme'
    };
    return theme;
}

const initialState = {
    news: "",
    newstype: "",
    newsList: [],
    newsDetail: {},
    isComment: {
        focus: false,
        placeholder: '新增留言',
        content: ""
    },
    isIntoNote: false,
    replyComment: [],
    theme: getTheme()
}
const SET_INIT = 'SET_INIT';
const SET_NEWSLIST = 'SET_NEWSLIST';
const SET_COMMENTINPUT = 'SET_COMMENTINPUT';
const SET_THEME = 'SET_THEME';
const SET_INTONOTE = 'SET_INTONOTE';
const SET_REPLYCOMMENT = 'SET_REPLYCOMMENT';
const SET_NEWSDETAIL = 'SET_NEWSDETAIL';

export const SetInit = (data) => {
    return {
        type: SET_INIT,
        data
    }
}

export const SetNewsList = (data) => {
    return {
        type: SET_NEWSLIST,
        newsList: data,
        isIntoNote: false,
        replyComment: []
    }
}

export const SetNewsDetail = (data) => {
    return {
        type: SET_NEWSDETAIL,
        newsDetail: data
    }
}

export const SetCommentInput = (data) => {
    return {
        type: SET_COMMENTINPUT,
        isComment: data
    }
}

export const SetTheme = (data) => {
    return {
        type: SET_THEME,
        theme: data
    }
}

export const SetIntoNote = (data) => {
    return {
        type: SET_INTONOTE,
        isIntoNote: data
    }
}
export const SetReplyComment = (data) => {
    return {
        type: SET_REPLYCOMMENT,
        replyComment: data
    }
}

export default function myNews(state = initialState, action) {
    switch (action.type) {
        case SET_INIT:
            return {
                ...state,
                ...action.data
            }
        case SET_THEME:
        case SET_COMMENTINPUT:
        case SET_NEWSLIST:
        case SET_NEWSDETAIL:
        case SET_INTONOTE:
        case SET_REPLYCOMMENT:
            return {
                ...state,
                ...action
            }
        default:
            return state;
    }
}