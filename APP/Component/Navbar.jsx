import React, { useState, useEffect } from 'react';
import { NavLink } from "react-router-dom"
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { SetNewsDetail } from '../Reducer/myNews';


const NavDiv = styled.div`
    position: sticky;
    z-index: 10;
    display: flex;
    justify-content: space-between;
    top: 0px;
    background : ${props => props.theme.containerColor};
    ul {
        margin: 15px 0 0 0;
        flex-direction: row;
        display: flex;
        list-style: none;
        li {
            height: 35px;
            width: 60px;
            text-align: center;
        }
        a {
            text-decoration: none;
            color: #b7b7b7;
            &:hover {
                color: ${props => props.theme.fontColor};
            }
            &.active {
                color: ${props => props.theme.fontColor};
                > li {
                    font-weight: bold;
                    border-bottom: 3px solid;
                }
            }
        }
    }
`;

const NavMenu = styled.div`
    position: absolute;
    top: 50px;
    background: rgba(0, 0, 0, 0.8);
    width: 100%;
    height: 100vh;
    ul {
        display: inline-block;
        a {
            border: 1px solid #e8e8e8;
            text-decoration: none;
            border-radius: 20px;
            margin: 0 8px 12px 0;
            min-width: 72px;
            min-height: 40px;
            display: inline-block;
            li {
                padding: 2px 0 0 0;
                display: flex;
                justify-content: center;
                align-items: center;
                width: unset;
            }
            &.active {
                background: ${props => props.theme.navBarMenuBg}; 
                > li {
                    color: #fff;
                    font-weight: none;
                    border: none;
                }
            }
        }
    }
`;

const NavMenuContainer = styled.div`
    background: ${props => props.theme.containerColor};
    width: 100%;
    border-radius: 0px 0px 7px 7px;
    border-top: 2px solid #b7b7b7; 
`;

const LabelText = styled.label`
    color : ${props => props.theme.fontColor};
`;

const ButtonNavMenu = styled.button`
    background-color: transparent;
    border-style: none;
    padding: 0;
    margin: 0;
    cursor: pointer;
    transform : ${props => props.haveClick ? 'rotate(135deg)' : 'rotate(-45deg)'};
    &:after{
        content : "∟";
        color : ${props => props.theme.fontColor}
    }
`;

export const MyNewsNavbar = () => {
    const dispatch = useDispatch();
    const [navMenuVisiable, setNavMenuVisiable] = useState(false);

    const ClickTab = () => {
        dispatch(SetNewsDetail({}));
    }
    const navMenuClick = () => {
        setNavMenuVisiable(!navMenuVisiable);
    }
    return (
        <NavDiv>
            {
                !navMenuVisiable &&
                <ul>
                    <NavLink to="/news/focus" onClick={ClickTab}><li>焦點</li></NavLink>
                    <NavLink to="/news/entertainment" onClick={ClickTab}><li>娛樂</li></NavLink>
                    <NavLink to="/news/domestic" onClick={ClickTab}><li>國內</li></NavLink>
                </ul>
            }
            {
                navMenuVisiable &&
                <>
                    <ul>
                        <li><LabelText>類別</LabelText></li>
                    </ul>
                    <NavMenu onClick={navMenuClick}>
                        <NavMenuContainer>
                            <ul>
                                <NavLink to="/news/focus" onClick={ClickTab}><li>焦點</li></NavLink>
                                <NavLink to="/news/entertainment" onClick={ClickTab}><li>娛樂</li></NavLink>
                                <NavLink to="/news/domestic" onClick={ClickTab}><li>國內</li></NavLink>
                            </ul>
                        </NavMenuContainer>
                    </NavMenu>
                </>
            }
            <ul>
                <li>
                    <ButtonNavMenu haveClick={navMenuVisiable} onClick={navMenuClick}></ButtonNavMenu>
                </li>
            </ul>
        </NavDiv>
    )
}