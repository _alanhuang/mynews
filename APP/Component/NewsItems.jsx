import React from 'react';
import styled from 'styled-components';

const NewsDiv = styled.div`
    cursor:pointer;
    display: flex;
    margin: ${props => props.haveNum ? '' : '20px 32px'};
`;

const NewsImg = styled.div`
    width: 200px;
    height: 100px;
`;

const NewsNum = styled.div`
    position: absolute;
    background: #ffa50099;
    padding: 0px 5px;
    border-radius: 5px;
    color : #fff
`;

const NewsText = styled.div`
    color: ${props => props.theme.fontColor};
    font-size: 18px;
    margin-left: 20px;
`;

const NewsTextSmall = styled.div`
    margin-top: 5px;
    font-size: 14px;
    color: #777;
`;

export const NewsItems = ({ index, id, imgSrc, title, creatorFrom, haveNum, GoNewsDeatil }) => {
    return (
        <NewsDiv haveNum={haveNum} /*onClick={() => { GoNewsDeatil(id) }}*/>
            <NewsImg>
                {haveNum && <NewsNum>{(index + 1).toString().padStart(2, '0')}</NewsNum>}
                <img src={imgSrc} height="80"></img>
            </NewsImg>
            <NewsText>
                <div>{title}</div>
                <NewsTextSmall>{creatorFrom}</NewsTextSmall>
            </NewsText>
        </NewsDiv>
    )
}