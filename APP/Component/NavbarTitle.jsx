import React from 'react';
import { NavLink } from "react-router-dom"
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { SetNewsDetail, SetTheme, SetNewsList } from '../Reducer/myNews';
import { getApi } from '../lib/common';
import { currentNewsType, SetStorages } from '../lib/common';
import { CommonButton } from './';
import { Outlet } from "react-router-dom"

const navLinkStyle = {
    textDecoration: 'none',
    color: 'black'
}
const NavbarTitleDiv = styled.div`
    cursor : pointer;
    &::before {
        content : "${props => props.isHome ? 'MyNews' : '\\1F3E0'}";
        color : ${props => props.theme.fontColor}
    }
`;

const NavbarUl = styled.ul`
    list-style: none;
    display: flex;
    padding-left: 1rem;
    margin : 0px;
    li {
        font-size: 25px;
        font-weight: bold;
        padding: 5px 0px 5px 0px;
        margin-left : 1rem;
        color : ${props => props.theme.fontColor}
    }
`;

const Container = styled.div`
    background-color: ${props => props.theme.containerColor};
    max-width: 1024px;
    margin: auto;
`;

export const NavbarTitle = (props) => {
    const dispatch = useDispatch();
    const MyNewsType = useSelector(state => state.myNews.newsDetail.type);
    const isIntoNote = useSelector(state => state.myNews.isIntoNote);
    const replyList = useSelector(state => state.myNews.replyComment);
    const newsDetail = useSelector(state => state.myNews.newsDetail);

    const ClickTab = () => {
        let result = getApi('/NewsServ/api/news/getnews');
        if (result.length > 0) {
            dispatch(SetNewsList(result));
        }
        dispatch(SetNewsDetail({}));
    }

    const ChangeBg = (type) => {
        SetStorages('theme', type);
        dispatch(SetTheme(type));
    }

    return (
        <Container>
            <NavbarUl>
                <li>
                    <NavLink to="/news/focus" style={navLinkStyle} onClick={ClickTab}>
                        {/* <NavbarTitleDiv isHome={props.isHome} /> */}
                        <NavbarTitleDiv isHome={Object.keys(newsDetail).length == 0 ? true : false} />
                    </NavLink>
                </li>
                {MyNewsType &&
                    <li>
                        {isIntoNote ? `留言(${replyList.length})` : currentNewsType(MyNewsType)}
                    </li>
                }

                <li>
                    <CommonButton bg="black" mr={2} width={20} height={20} border='1px solid #777' title="深色" onClick={() => ChangeBg('darkTheme')}></CommonButton>
                    <CommonButton bg="white" mr={2} width={20} height={20} border='1px solid #777' title="正常" onClick={() => ChangeBg('whiteTheme')}></CommonButton>
                </li>
            </NavbarUl>
            <Outlet />
        </Container>
    )
}