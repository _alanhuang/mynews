import React from 'react';
import styled from 'styled-components';
import { color, space, layout, border } from 'styled-system';

const ButtonStyle = styled.button`
    ${color};
    ${space};
    ${layout};
    ${border};
    cursor: pointer;
`;

export const CommonButton = (props) => {
    return (
        <ButtonStyle {...props} />
    )
}