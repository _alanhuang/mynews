export const ThemeStyle = {
    whiteTheme: {
        bodyBg: '#f5f5f5',
        containerColor: '#fcfcfc',
        fontColor: '#000',
        newsHeaderBg: '#FFF',
        navBarMenuBg: '#000',
        boxShadow: 'rgba(0, 0, 0, 0.04)'
    },
    darkTheme: {
        bodyBg: '#000',
        containerColor: '#262626',
        fontColor: '#FFF',
        newsHeaderBg: '#777',
        navBarMenuBg: '#777',
        boxShadow: 'rgba(255, 255, 255, 0.04)'
    }
}