import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    body{
        margin : 0;
        font-family: "Noto Sans TC", "Microsoft JhengHei", "微軟正黑體", "Arial";
        background-color: ${props => props.theme.bodyBg};
    }
    .pointer {
        cursor: pointer;
    }
    small {
        color: #777;
        font-size: 12px;
    }
`;