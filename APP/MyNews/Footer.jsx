import React from 'react';
import styled from 'styled-components';

const FooterDiv = styled.div`
padding : 24px 32px;
text-align : center;
> span {
    font-size : 13px;
    color : #b7b7b7;
}
`;

export const Footer = () => {
    return (
        <FooterDiv>
            <span>© MyNews Corporation</span>
        </FooterDiv>
    )
}