import React from 'react';
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { SetNewsDetail } from '../Reducer/myNews';
import { NewsItems } from '../Component';
import { getApi } from '../lib/common';
import styled from 'styled-components';
import { Link } from "react-router-dom"

const NewsHeader = styled.div`
    background: ${props => props.theme.newsHeaderBg};
    color: ${props => props.theme.fontColor};
    font-weight: bold;
    border-radius: 5px;
    display: flex;
    margin: -43px 32px 0px 32px;
    padding: 0px 20px;
    z-index: 1;
    position: relative;
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.04);
`;

const NewsLink = styled(Link)`
    text-decoration: none;
`;

export const Header = () => {
    const dispatch = useDispatch();
    const newsList = useSelector(state => state.myNews.newsList);
    const params = useParams();

    let newsTypeList;
    const GoNewsDeatil = (id) => {
        let result = getApi('/NewsServ/api/News/GetNewsDetail', { id: id });
        if (result.length > 0) {
            dispatch(SetNewsDetail(result[0]));
        }
    }

    if (newsList.length > 0) {
        newsTypeList = newsList.filter(item => item.type == params.newstype);
    }

    return (
        <div>
            {newsTypeList && newsTypeList.map((items, index) => {
                if (index == 0) {
                    let imgSrc = require(`../assets/images/news/${items.type}/${items.image}`);
                    return (
                        <NewsLink key={items.id} to={`/news/${items.type}/${items.id}`}/* onClick={() => GoNewsDeatil(items.id)}*/>
                            <div className='pointer'>
                                <img src={imgSrc} width="100%"></img>
                                <NewsHeader>
                                    <h3>{items.title}</h3>
                                </NewsHeader>
                            </div>
                        </NewsLink>
                    )
                }
            })}
            {newsTypeList && newsTypeList.map((items, index) => {
                if (index > 0) {
                    let imgSrc = require(`../assets/images/news/${items.type}/${items.image}`);
                    return (
                        <NewsLink key={items.id} to={`/news/${items.type}/${items.id}`} onClick={() => GoNewsDeatil(items.id)}>
                            <NewsItems
                                id={items.id}
                                index={index}
                                imgSrc={imgSrc}
                                title={items.title}
                                creatorFrom={items.creatorFrom}
                                haveNum={false}
                                // GoNewsDeatil={GoNewsDeatil}
                                GoNewsDeatil=""
                            />
                        </NewsLink>
                    )
                }
            })}
        </div>
    )
}