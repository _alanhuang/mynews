import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SetNewsDetail, SetCommentInput } from '../Reducer/myNews';
import { NewsItems } from '../Component';
import { currentNewsType } from '../lib/common';
import { getApi } from '../lib/common';
import { Link } from "react-router-dom"
import styled from 'styled-components';

const NewsLink = styled(Link)`
    text-decoration: none;
`;

class ContentMore extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newsTypeText: '',
            moreNewsList: []
        }
    }

    GoNewsDeatil = (id) => {
        let result = getApi('/NewsServ/api/News/GetNewsDetail', { id: id });
        if (result.length > 0) {
            this.props.actions.SetNewsDetail(result[0]);
        }
        this.props.actions.SetCommentInput({ focus: false, placeholder: '新增留言', content: "" });
    }

    GetNewsList = () => {
        let newsDetail = this.props.newsDetail,
            newsList = this.props.newsList,
            moreNewsList = newsList.filter(items => { return items.type == newsDetail.type && items.id != newsDetail.id });

        let newsTypeText = `更多${currentNewsType(newsDetail.type)}相關文章`;

        this.setState({
            newsTypeText: newsTypeText,
            moreNewsList: moreNewsList
        });
    }

    componentDidMount() {
        this.GetNewsList();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.newsDetail.id != this.props.newsDetail.id) {
            this.GetNewsList();
        }
    }

    render() {
        let { newsTypeText, moreNewsList } = this.state;
        return (
            <>
                <div>
                    <h2>{newsTypeText}</h2>
                </div>
                {moreNewsList && moreNewsList.map((items, index) => {
                    let imgSrc = require(`../assets/images/news/${items.type}/${items.image}`);
                    return (
                        <NewsLink key={items.id} to={`/news/${items.type}/${items.id}`} onClick={() => this.GoNewsDeatil(items.id)}>
                            <NewsItems
                                id={items.id}
                                index={index}
                                imgSrc={imgSrc}
                                title={items.title}
                                creatorFrom={items.creatorFrom}
                                haveNum={true}
                            //GoNewsDeatil={this.GoNewsDeatil}
                            />
                        </NewsLink>
                    )
                })}
            </>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        newsList: state.myNews.newsList,
        newsDetail: state.myNews.newsDetail,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        actions: bindActionCreators({ SetNewsDetail, SetCommentInput }, dispatch)
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps)(ContentMore);