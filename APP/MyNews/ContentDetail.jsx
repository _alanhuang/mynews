import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DateCalc } from '../lib/common';
import styled from 'styled-components';

const NewsTitle = styled.div`
    font-size: 2em;
    font-weight: bold;
`;
const NewsHeader = styled.div`
    margin-top: 30px;
    display: flex;
`;
const NewsIcon = styled.div`
    width: 50px;
    height: 50px;
    border-radius: 10em;
    border: 1px solid;
    font-weight: bold;
    > span {
        font-size: 35px;
        margin-left: 7px;
    }
`;

const NewsText = styled.div`
    font-size: 18px;
    line-height: 30px;
    margin-top : 1rem;
`;

class ContentHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            createDate: this.props.newsDetail.createDate,
            updateDate: this.props.newsDetail.updateDate
        }
    }

    componentDidMount() {
        let newsDetail = this.props.newsDetail;
        let createDate = DateCalc(newsDetail.createDate, '發布於');
        let updateDate = DateCalc(newsDetail.updateDate, '更新於');
        this.setState({
            createDate: createDate,
            updateDate: updateDate
        })
    }

    render() {
        let { createDate, updateDate } = this.state;
        return (
            <div>
                <NewsTitle>
                    {this.props.newsDetail.title}
                </NewsTitle>
                <NewsHeader>
                    <NewsIcon>
                        <span>{this.props.newsDetail.creator.substr(0, 1)}</span>
                    </NewsIcon>
                    <div className='ml-3'>
                        <div><b>{this.props.newsDetail.creator}</b></div>
                        <small className='mt-1'>{updateDate}，{createDate}</small>
                    </div>
                </NewsHeader>
                <NewsText>
                    {this.props.newsDetail.content}
                </NewsText>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        newsList: state.myNews.newsList,
        newsDetail: state.myNews.newsDetail,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {

    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps)(ContentHeader);