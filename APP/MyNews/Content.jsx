import React, { Component } from 'react';
import { connect } from 'react-redux';
import ContentDetail from './ContentDetail.jsx';
import ContentMore from './ContentMore.jsx';
import { ContentNote } from './ContentNote.jsx';
import styled from 'styled-components';
import { ContentBottomBar } from './ContentBottomBar.jsx';
import withRouter from '../lib/withRouter'
import { getApi } from '../lib/common';
import { bindActionCreators } from 'redux';
import { SetNewsDetail } from '../Reducer/myNews';

const NewsDetailDiv = styled.div`
    position: relative;
    padding: 30px;
    color : ${props => props.theme.fontColor}
`;

class Content extends Component {
    constructor(props) {
        super(props);
    }

    GetNewsDetail() {
        let result = getApi('/NewsServ/api/News/GetNewsDetail', { id: this.props.params.id });
        if (result.length > 0) {
            this.props.actions.SetNewsDetail(result[0]);
        }
    }

    componentDidMount() {
        this.GetNewsDetail();
    }

    componentDidUpdate(prevProps, nextProps) {
        if (prevProps.params.id != this.props.params.id) {
            this.GetNewsDetail();
        }
    }

    render() {
        let { isIntoNote, newsDetail } = this.props;
        return (
            <NewsDetailDiv>
                {(!isIntoNote && newsDetail.id) ?
                    <>
                        <ContentDetail />
                        <ContentMore />
                        <ContentNote />
                        <ContentBottomBar />
                    </>
                    :
                    <>
                        <ContentNote />
                    </>
                }
            </NewsDetailDiv>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isIntoNote: state.myNews.isIntoNote,
        newsDetail: state.myNews.newsDetail
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        actions: bindActionCreators({ SetNewsDetail }, dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps)(withRouter(Content)); 