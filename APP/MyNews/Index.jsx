import React, { useEffect, Component } from 'react';
import { MyNewsNavbar } from '../Component';
import { Header } from './Header.jsx';
import { Footer } from './Footer.jsx';
import { useDispatch, useSelector } from 'react-redux';
import { SetNewsDetail } from '../Reducer/myNews';

export const MyNewsIndex = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(SetNewsDetail({}));
    }, [])

    return (
        <div>
            <MyNewsNavbar />
            <Header />
            <Footer />
        </div>
    )
}