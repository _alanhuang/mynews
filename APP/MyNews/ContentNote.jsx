import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { SetReplyComment } from '../Reducer/myNews';
import { DateDiff, DateCalc } from '../lib/common';
import styled from 'styled-components';
import { space, layout, border, flexbox, variant } from 'styled-system';

const Container = styled.div`
    ${space}
    ${border}
    ${layout}
    ${flexbox}
`;

const CommentUl = styled.ul`
    ${space}
    list-style: none;
`;

const CommentControll = styled.div`
    display: flex;
    align-items: center;
    justify-content: flex-end;
`;

const CommentInput = styled.textarea`
    ${layout}
    color: ${props => props.theme.fontColor};
    background-color: transparent;
    outline: none;
    resize: none;
    border: none;
    font-weight: 400;
    font-size: 15px;
    margin: 14px 0;
`;

const InputCount = styled.div`
    color: #b7b7b7;
    font-size: 14px;
`;

const InputBtn = styled.button`
    width: 60px;
    height: 45px;
    color: #06c755;
    font-size: 15px;
    font-weight: bold;
    background-color: transparent;
    border-style: none;
    padding: 0;
    margin: 0;
    cursor: pointer;
    &:disabled {
        color: #c8c8c8;
        cursor: not-allowed;
    }
`;

const TabBtn = styled.button`
    position: relative;
    font-size: 13px;
    background-color: transparent;
    border-style: none;
    padding: 0;
    margin-right: 14px;
    cursor: pointer;
    ${props => props.isActive ?
        `
            color: ${props.theme.fontColor};
            font-weight: bold;
         `
        :
        `color: #b7b7b7;`
    }
    &::after {
        content: "";
        position: absolute;
        top: 50%;
        right: -8px;
        margin-top: -5px;
        background-color: #e8e8e8;
        width: 1px;
        height: 10px;
    }
`;

const CommentItems = styled.div`
    ${space}
    padding: 12px 16px;
    border: 1px solid #e8e8e8;
    border-radius: 3px;
`;

const CommentContent = styled.div`
    padding: 0 0 5px;
    font-size: 15px;
    line-height: 1.2em;
    ${variant({
    variants: {
        head: {
            fontWeight: 'bold'
        },
        content: {
            wordBreak: 'break-all'
        }
    }
})}
`;

const CommentBtn = styled.button`
    color: ${props => props.theme.fontColor};
    border: 1px solid #e8e8e8;
    border-radius: 1px;
    font-size: 13px;
    line-height: 23px;
    padding: 1px 7px 0;
    cursor: pointer;
    background-color: transparent;
`;

const CommentVoteBtn = styled(CommentBtn)`
    display : flex;
    > div {
        font-weight: bold;
        position: relative;
        top: ${props => props.type == "UP" ? '2px' : '-2px'};
        transform: ${props => props.type == "UP" ? 'rotate(135deg)' : 'rotate(-45deg)'};
    }
`;

export const ContentNote = () => {
    const dispatch = useDispatch();
    const newsDetail = useSelector(state => state.myNews.newsDetail);
    const replyList = useSelector(state => state.myNews.replyComment);
    const isIntoNote = useSelector(state => state.myNews.isIntoNote);
    const [commentType, setCommentType] = useState('hot');

    let isCommentInit = {
        focus: false,
        placeholder: '新增留言',
        content: "",
        reply: replyList.map(item => { return { replyComment: '' } })
    };
    const [isComment, setIsComment] = useState(isCommentInit);

    useEffect(() => {
        setIsComment(isCommentInit);
        if (!isIntoNote) {
            dispatch(SetReplyComment([]));
        }
    }, [newsDetail])


    const InputEvent = () => {
        setIsComment({ ...isComment, focus: true, placeholder: '歡迎留言，請避免不雅字眼。' });
    }

    const CountCalc = (v) => {
        setIsComment({ ...isComment, content: v.target.value });
    }

    const ReplyCountCalc = (event, index) => {
        isComment.reply[index].replyComment = event.target.value;
        setIsComment({ ...isComment });
    }

    const AddComment = () => {
        isComment.reply.push({
            replyComment: ''
        })
        setIsComment({
            ...isComment,
            focus: false,
            placeholder: '新增留言',
            content: ""
        });


        replyList.push({
            focus: false,
            replyContent: "",
            good: 0,
            bad: 0,
            name: "沒有Name",
            content: isComment.content,
            createDate: new Date(),
            reply: []
        })

        dispatch(SetReplyComment([...replyList]));
    }

    const AddReplyComment = (index) => {
        replyList[index].replyContent = '';
        replyList[index].reply.push({
            name: "回覆也沒有Name",
            content: isComment.reply[index].replyComment,
            createDate: new Date(),
        })
        dispatch(SetReplyComment([...replyList]));


        isComment.reply[index].replyComment = '';
        setIsComment({ ...isComment });

    }

    const ClickReplyBtn = (index) => {

        replyList[index].focus = !replyList[index].focus
        dispatch(SetReplyComment([
            ...replyList
        ]));

    }

    const CommentBtnClick = (type) => {
        let newAry = [...replyList];
        switch (type) {
            case "hot":
                newAry.sort(function (a, b) {
                    return b.reply.length - a.reply.length;
                });
                break;
            case "focus":
                newAry.sort(function (a, b) {
                    return b.good - a.good;
                });
                break;
            case "new":
                newAry.sort(function (a, b) {
                    let aDate = parseInt(DateDiff(new Date(b.createDate), new Date(), 's'));
                    let bDate = parseInt(DateDiff(new Date(a.createDate), new Date(), 's'));
                    return bDate - aDate;
                });
                break;
        }
        setCommentType(type);
        dispatch(SetReplyComment(newAry));

    }

    const ClickGoodBtn = (index) => {
        replyList[index].good = replyList[index].good + 1;
        dispatch(SetReplyComment([...replyList]));
    }

    const ClickBadBtn = (index) => {
        replyList[index].bad = replyList[index].bad + 1;
        dispatch(SetReplyComment([...replyList]));
    }

    return (
        <div>
            {!isIntoNote &&
                <>
                    <div>
                        <h4>留言 {replyList.length}</h4>
                    </div>
                    <hr></hr>
                </>
            }
            <Container display='flex' p='0 16px' flexDirection={isComment.focus ? 'column' : 'initial'}>
                <div className='width-10'>
                    <CommentInput width='100%' height={isComment.focus ? 135 : ''} placeholder={isComment.placeholder} onClick={InputEvent} onChange={CountCalc} value={isComment.content} maxLength="500"></CommentInput>
                </div>
                <CommentControll>
                    {isComment.focus && <InputCount>{isComment.content.length}/500</InputCount>}
                    <InputBtn disabled={isComment.content == ""} onClick={AddComment}>傳送</InputBtn>
                </CommentControll>
            </Container>
            <Container display='flex' p='0 16px'>
                <TabBtn isActive={commentType == "hot"} onClick={() => { CommentBtnClick("hot") }}>熱門</TabBtn>
                <TabBtn isActive={commentType == "focus"} onClick={() => { CommentBtnClick("focus") }}>最多關注</TabBtn>
                <TabBtn isActive={commentType == "new"} onClick={() => { CommentBtnClick("new") }}>最新</TabBtn>
            </Container>

            <CommentUl p='0 10px'>
                {replyList.length > 0 &&
                    replyList.map((items, index) => {
                        let createDate = DateCalc(items.createDate);

                        return (
                            <li key={`content-${index}`}>
                                <CommentItems m='0 24px 8px 8px'>
                                    <CommentContent variant='head'>{items.name}</CommentContent>
                                    <CommentContent variant='content'>
                                        {items.content.split('\n').map((item, idx) => {
                                            return (
                                                <span key={idx}>
                                                    {item}
                                                    <br></br>
                                                </span>
                                            )
                                        })}
                                    </CommentContent>
                                    <small>{createDate}</small>
                                    <Container display='flex' justifyContent='space-between' p='13px 0 5px'>
                                        <CommentBtn className='newsCommentBtn' onClick={() => ClickReplyBtn(index)}>回覆 {items.reply.length}</CommentBtn>
                                        <Container display='flex'>
                                            <CommentVoteBtn className='ml-2' type='UP' onClick={() => ClickGoodBtn(index)}>
                                                <div>∟</div>
                                                <label>{items.good}</label>
                                            </CommentVoteBtn>
                                            <CommentVoteBtn className='ml-2' type='Down' onClick={() => ClickBadBtn(index)}>
                                                <div>∟</div>
                                                <label>{items.bad}</label>
                                            </CommentVoteBtn>
                                        </Container>
                                    </Container>
                                    {items.focus &&
                                        <div>
                                            <CommentUl p={0}>
                                                {items.reply.length > 0 &&
                                                    items.reply.map((items, index) => {
                                                        let replyCreateDate = DateCalc(items.createDate);
                                                        return (
                                                            <li key={`reply-${index}`}>
                                                                <CommentItems m='0 24px 8px 0'>
                                                                    <CommentContent variant='head'>{items.name}</CommentContent>
                                                                    <CommentContent variant='content'>
                                                                        {items.content.split('\n').map((item, idx) => {
                                                                            return (
                                                                                <span key={idx}>
                                                                                    {item}
                                                                                    <br></br>
                                                                                </span>
                                                                            )
                                                                        })}
                                                                    </CommentContent>
                                                                    <small>{replyCreateDate}</small>
                                                                </CommentItems>
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </CommentUl>
                                            <Container p='0 10px' border='1px solid #e8e8e8' borderRadius='3px'>
                                                <CommentInput width='100%' height={75} onChange={(e) => ReplyCountCalc(e, index)} value={isComment.reply[index] && isComment.reply[index].replyComment} maxLength="500"></CommentInput>
                                                <CommentControll>
                                                    <InputCount>{isComment.reply[index] && isComment.reply[index].replyComment.length}/500</InputCount>
                                                    <InputBtn disabled={isComment.reply[index] && isComment.reply[index].replyComment == ""} onClick={() => AddReplyComment(index)}>傳送</InputBtn>
                                                </CommentControll>
                                            </Container>
                                        </div>
                                    }
                                </CommentItems>
                            </li>
                        );
                    })
                }
            </CommentUl>
        </div>
    )
}