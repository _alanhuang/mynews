import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { SetNewsDetail, SetIntoNote } from '../Reducer/myNews';
import styled from 'styled-components';
import { layout } from 'styled-system';
import comment from '../assets/images/comment.svg'
import heart from '../assets/images/heart.svg'
import home from '../assets/images/home.svg'
import commentWhite from '../assets/images/commentWhite.svg'
import heartWhite from '../assets/images/heartWhite.svg'
import homeWhite from '../assets/images/homeWhite.svg'
import { useNavigate } from 'react-router-dom';

const Container = styled.div`
    ${layout}
`;

const BottomDiv = styled.div`
    transform: translateY(0); 
    transition:all 0.5s ease;
    position: fixed;
    bottom: 0px;
    width: 100%;
    left: 0px;
    &.bottomDivHide { 
        transform: translateY(100%); 
        transition:all 0.5s ease; 
    }   
`;

const BottomContainer = styled.div`
    box-shadow: 0 -3px 5px 0 ${props => props.theme.boxShadow};
    max-width: 1024px;
    margin: auto;
    background: ${props => props.theme.containerColor};
`;

const BottomBar = styled.div`
    display: flex;
    padding: 5px 10px 5px 10px;
    margin-left : 1rem;
    margin-right : 1rem;
    justify-content: space-between;
`;

const BottomBarLeft = styled.div`
    display : flex;
    align-items: flex-end;
    padding : 5px;
    cursor : pointer;
    margin-right : 1rem;
    > span {
        font-weight : bold;
    }
    &:hover {
        background : rgba(0, 0, 0, 0.04);
    }
`;

const BottomBarRight = styled.div`
    cursor : pointer;
    padding : 5px;
    &:hover {
        background : rgba(0, 0, 0, 0.04);
    }
    &::before{
        content: '';
        border-right: 1px solid #e8e8e8;
        margin-right: 0.5rem;
    }
`;

export const ContentBottomBar = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const replyList = useSelector(state => state.myNews.replyComment);
    const theme = useSelector(state => state.myNews.theme);
    const [heartCount, setHeartCount] = useState(0);
    const bottomBarRef = useRef(0);

    const iconSvg = {
        whiteTheme: {
            heart: heart,
            comment: comment,
            home: home,
        },
        darkTheme: {
            heart: heartWhite,
            comment: commentWhite,
            home: homeWhite,
        }
    }

    const changeImage = (type) => {
        if (theme == '') {
            return iconSvg['whiteTheme'][type];
        }
        return iconSvg[theme][type];
    }

    useEffect(() => {
        let scrollY = 0;
        let timeout;
        const handleScroll = () => {
            if (scrollY > window.scrollY) {
                bottomBarRef.current.classList.remove('bottomDivHide')
            } else {
                bottomBarRef.current.classList.add('bottomDivHide')
            }
            scrollY = window.scrollY;

            clearTimeout(timeout);
            timeout = setTimeout(() => {
                if (bottomBarRef.current) {
                    bottomBarRef.current.classList.remove('bottomDivHide')
                }
            }, 1500)
        };
        window.addEventListener("scroll", handleScroll);
        return () => {
            window.removeEventListener("scroll", handleScroll);
        }
    }, []);



    return (
        <BottomDiv ref={bottomBarRef}>
            <BottomContainer>
                <BottomBar>
                    <Container display='flex'>
                        <BottomBarLeft onClick={() => setHeartCount(heartCount + 1)}>
                            <div><img src={changeImage('heart')} widht="25" height="25" /></div>
                            <span>{heartCount}</span>
                        </BottomBarLeft>
                        <BottomBarLeft onClick={() => dispatch(SetIntoNote(true))}>
                            <div><img src={changeImage('comment')} widht="25" height="25" /></div>
                            <span>{replyList.length}</span>
                        </BottomBarLeft>
                    </Container>
                    <Container display='flex'>
                        <BottomBarRight onClick={() => { dispatch(SetNewsDetail({})); navigate("/"); }}>
                            <img src={changeImage('home')} widht="25" height="25" />
                        </BottomBarRight>
                    </Container>
                </BottomBar>
            </BottomContainer>
        </BottomDiv >
    )
}