import React, { Component, useEffect } from 'react';
import { Provider } from 'react-redux';
import { createRoot } from 'react-dom/client';
import { MyNewsIndex } from './mynews/Index.jsx';
import Content from './mynews/Content.jsx';
import '../scss/WebStyle.scss'
import { HashRouter, Routes, Route, Navigate, Outlet } from "react-router-dom"
import { SetNewsList } from './Reducer/myNews.js';
import { configureStore } from './Reducer/MyNewsStore.js';
import { getApi } from './lib/common';
import { NavbarTitle } from './Component/';
import { ThemeProvider } from 'styled-components';
import { ThemeStyle, GlobalStyle } from './mynews/Styled';
import { connect } from 'react-redux';

const MyNewsReducer = configureStore();

class MyNews extends Component {
    constructor(props) {
        super(props);
    }

    getNewList = async () => {
        let result = getApi('/NewsServ/api/news/getnews');
        if (result.length > 0) {
            MyNewsReducer.dispatch(SetNewsList(result));
        }
    }

    componentDidMount() {
        this.getNewList();
    }

    render() {
        const theme = ThemeStyle[this.props.theme];
        return (
            <ThemeProvider theme={theme}>
                <GlobalStyle />
                <HashRouter>
                    <Routes>
                        <Route path="/" element={<NavbarTitle />}>
                            <Route path="/" element={<Navigate replace to="/news/focus" />} />
                            <Route path="/news/:newstype" element={<MyNewsIndex />} />
                            <Route path="/news/:newstype/:id" element={<Content />} />
                        </Route>
                    </Routes>
                </HashRouter>
            </ThemeProvider>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        theme: state.myNews.theme,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

export const MyNewsMain = connect(
    mapStateToProps,
    mapDispatchToProps)(MyNews);

const root = createRoot(document.getElementById('APP'));
root.render(
    <Provider store={MyNewsReducer}>
        <MyNewsMain />
    </Provider>
)